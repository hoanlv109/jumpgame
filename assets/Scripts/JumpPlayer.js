cc.Class({
    extends: cc.Component,

    properties: {
       power: 0,
       initSpeed: 0,
       jumpDistance: 0
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.isReadyJump = false;
        this.direction = 1;
        this.speed = this.initSpeed;
    },

    onScreenTouchEnd() {
        return;
    },

    readyJump() {
        this.isReadyJump = true;
        this.node.runAction(cc.scaleTo(PLAYER_DF.JUMP.SCALE_TIME, PLAYER_DF.JUMP.SCALE_X, PLAYER_DF.JUMP.SCALE_Y));
    },

    jumpTo(targetPos, callback, cbtarget) {
        this.node.stopAllActions();
        console.log("ddđ");
        this.isReadyJump = false;
        let resetAction = cc.scaleTo(1,1,1);
        let jumpAction = cc.jumpTo(0.5,targetPos,PLAYER_DF.JUMP.JUMP_HEIGHT,1);
        // let rotateAction = cc.rotateBy(0.5,this.direction*360);
        let finished = cc.callFunc(()=>{
            this.speed = 0;
            this.jumpDistance = 0;
            callback();
        },cbtarget);
        this.node.runAction(resetAction);
        this.node.runAction(cc.sequence(jumpAction,finished))
    },

    update (dt) {
        if(this.isReadyJump) {
            this.speed += dt * this.power;
            this.jumpDistance += this.speed * dt;
        }
    },
});
