window.PLAYER_DF = {
    JUMP:{
        SCALE_TIME: 2,
        SCALE_X: 1,
        SCALE_Y: 0.5,
        JUMP_HEIGHT: 200
    },
}

window.STATE = {
    NONE: 0,
    READY: 1,
    START: 2,
    OVER: 3
}
