var JumpStage = require("JumpStage");
cc.Class({
    extends: cc.Component,

    properties: {
        //stage: cc.Node,
    },


    onLoad () {
        this.stage = JumpStage.instance;
        this.addListeners();
        this.startGame();
    },

    startGame() {
        this.state = STATE.START;
        this.stage.init(this);
        this.stage.startGame();
    },

    addListeners() {
        this.node.on(cc.Node.EventType.TOUCH_START, this.onScreenTouchStart, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this.onScreenTouchEnd, this);
    },

    onScreenTouchStart() {
        console.log("aaaaaa");
        if (this.state == STATE.START) {
            console.log("aaaaaa111");
            this.stage.playerReadyJump();
        }
    },

    onScreenTouchEnd() {
        console.log("bbbbb");
        if (this.state === STATE.START) {
            console.log("bbbbb111");
            this.stage.playerJump((playerIndex, needNewBlock) => {
                // if (playerIndex === -1) {
                //     this.state = STATE.NONE;
                //     this.stage.playerDie(()=>{
                //         this.overGame(false);
                //     },this);
                // } else 
                {
                    // if (isSuccess) {
                    //     this.overGame(true);
                    // } else 
                    {
                        if (needNewBlock) {
                            this.stage.addNewBlock();
                        }
                    }
                }
            }, this);
        }
    }



});
