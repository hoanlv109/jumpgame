var Block = cc.Class({
    extends: cc.Component,

    properties: {
    
    },

    ctor (head, length) {
        this.head = head,
        this.length = length
    },

    getTail() {
        return this.head+this.length-1;
    }
  
});
module.exports = new Block();
