
var JumpStage = cc.Class({
    extends: cc.Component,

    properties: {
      player: cc.Node,
      blockPrefab: cc.Prefab,
      groundLayer: cc.Node,
    },

    statics: {
        instance: null
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        JumpStage.instance = this;
        this.reset();
        this.gridWidth = 300/2;
        this.cameraMoveDuration =1 ;
    },

    startGame () {

    },

    init(jumpScene) {
        this.jumpScene = jumpScene;
    },

    reset() {
        this.currBlock = cc.instantiate(this.blockPrefab);
        this.currBlock.parent = this.node;
        this.nextBlock = cc.instantiate(this.blockPrefab);
        this.nextBlock.parent = this.node;
        this.currBlock.position = cc.v2(200 ,200);
        //this.drawBlock(this.nextBlock);
        let center = this.currBlock.head + Math.floor(this.currBlock.length / 2);
        this.playerIndex = center;
       // this.player.position = cc.v2(center * this.gridWidth + this.gridWidth / 2, 0);
        this.isMovingStage = false;
    },

    canReadyJump() {
        return !this.isMovingStage && this.player.speed === 0;
    },

    playerReadyJump() {
        //if (this.canReadyJump()) {
            this.player.getComponent("JumpPlayer").readyJump();
        //}
    },

    playerJump(cb, target) {
        if (!this.player.getComponent("JumpPlayer").isReadyJump) {
            console.log("cccccc222");
            return;
        }
        
        let jumpGrids = Math.round(this.player.jumpDistance / this.gridWidth);
        let targetGrid = this.playerIndex + jumpGrids;
        let targetPos = cc.v2(-5,239);
        console.log("cccccc");
        this.player.getComponent("JumpPlayer").jumpTo(targetPos, () => {
           this.playerIndex = targetGrid;
           this.moveStage(cb, this);
            // if (targetGrid <= this.currBlock.tail) {
            //     cb(this.playerIndex, false);
            // } else if (targetGrid >= this.nextBlock.head && targetGrid <= this.nextBlock.tail) {
               // cb(this.playerIndex, true);
            // } else {
            //     cb(-1, false);
            // }
        }, this);
    },

    moveStage(cb, target) {
        this.isMovingStage = true;
        let moveLength = 150;
        let moveAction = cc.moveBy(this.cameraMoveDuration, 0, -moveLength);
        let action = cc.sequence(moveAction, cc.callFunc(() => {
            this.isMovingStage = false;
            if(cb)
                cb();
        }, target));
        this.groundLayer.runAction(action);
    }


    

    // update (dt) {},
});
